let form = ({
    rowTemplate: null,
    init() {
        $('section.columns .column select').change(form.setTypeClass).change();
        $('section.columns .column .form-check').each(function (idx, el) {
            form.initDelete($(el))
        })
        $('.add-btn').click(form.addNew);
        form.rowTemplate = $('section.columns .column.add-new').clone();

    },
    getLength() {
        return $('section.columns .column').length;
    },
    addNew: function () {
        const REPLACE_PATTERN = /\d+/;
        const nextIndex = form.getLength();
        // Clear current add new row
        const lastRow = $('section.columns .column.add-new');
        lastRow.removeClass('add-new');
        lastRow.find('.add-btn').closest('.row').remove();

        const newRow = form.rowTemplate.clone();
        newRow.find('label').each(function (idx, label) {
            label = $(label);
            const forAttr = label.attr('for');
            if (forAttr) label.attr('for', forAttr.replace(REPLACE_PATTERN, nextIndex));
        });

        function replaceIDAndLabel(idx, input) {
            input = $(input);
            const idAttr = input.attr('id');
            const nameAttr = input.attr('name');
            if (idAttr) input.attr('id', idAttr.replace(REPLACE_PATTERN, nextIndex));
            if (nameAttr) input.attr('name', nameAttr.replace(REPLACE_PATTERN, nextIndex));
            input.removeClass('is-valid');
            input.removeClass('is-invalid');
        }

        newRow.find('input').each(replaceIDAndLabel);
        newRow.find('select').each(replaceIDAndLabel);
        newRow.insertAfter($('section.columns .column:last'));
        newRow.find($('.row.delete')).remove();
        form.initDelete(newRow.find('.form-check'));
        $('section.columns .column select').change(form.setTypeClass).change();
        newRow.find('.add-btn').click(form.addNew);
        const total = $('#id_columns-TOTAL_FORMS');
        total.val(parseInt(total.val()) + 1);
    },

    setTypeClass(event) {
        window.e = event;
        const input = $(event.target)
        if (!input.attr('name').match(/^columns-[\d]+-type$/)) return
        const options = $.map(input.find('option'), function (option) {
            return option.value;
        });
        const column = input.closest('.column');
        column.attr('class').split(/\s+/).forEach(function (cls) {
            if (options.includes(cls)) column.removeClass(cls);
        })
        column.addClass(input.children("option:selected").val());
    },
    initDelete(el) {
        el.hide();
        const link = $('<div class="row delete"><a href="#" class=" text-danger">Delete</a></div>');
        $(link).insertBefore(el);
        link.click(function (event) {
            event.preventDefault();
            el.find('input').prop('checked', true);
            link.closest('.column').hide()
        });
    }
});

$(document).ready(function () {
    form.init()
});