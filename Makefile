PROJECT=planeks
CONTAINER=web

build:
	docker-compose build

up:
	docker-compose -p $(PROJECT) up -d

stop:
	docker-compose -p $(PROJECT) stop

run: up
	docker-compose -p $(PROJECT) exec $(CONTAINER) ./manage.py runserver 0.0.0.0:8000

bash:
	docker-compose -p $(PROJECT) exec $(CONTAINER) bash

pipi:
	docker-compose -p $(PROJECT) exec $(CONTAINER) pip install -r ./requirements.txt

migrate:
	docker-compose -p $(PROJECT) exec $(CONTAINER) ./manage.py migrate

collectstatic:
	docker-compose -p $(PROJECT) exec $(CONTAINER) ./manage.py collectstatic

makemigrations:
	docker-compose -p $(PROJECT) exec $(CONTAINER) ./manage.py makemigrations

prospector:
	docker-compose -p $(PROJECT) exec $(CONTAINER) prospector --uses django
