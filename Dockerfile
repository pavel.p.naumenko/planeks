FROM python:3.9-slim
MAINTAINER Pavlo Naumenko <pavel.n@edicasoft.com>
RUN apt-get install gcc libpq-devel

ADD ./requirements.txt /app/
RUN cd /app && pip install -U -r requirements.txt
WORKDIR /app
CMD "/bin/bash"
