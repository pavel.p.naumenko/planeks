from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin, ProcessFormView
from django.forms import inlineformset_factory
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from fakecsv.forms import SchemaForm, SchemaColumnForm, DataSetForm
from fakecsv.models import Schema, SchemaColumn, DataSet, Status
from fakecsv.tasks import create_dataset

# Create your views here.
SchemaColumnsFormSet = inlineformset_factory(Schema, SchemaColumn, form=SchemaColumnForm, exclude=('id',), extra=1,
                                             can_delete=True)


class FilterQuerySetMixin:

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class SchemaColumnsMixin:

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['columns'] = self.get_formset()
        return context

    def get_formset(self):
        return SchemaColumnsFormSet(self.request.POST or None, instance=self.object)

    def form_valid(self, form):
        formset = self.get_formset()
        if not formset.is_valid():
            print(formset.errors)
            return self.render_to_response(self.get_context_data(form=form))

        schema = form.save(commit=False)
        schema.user = self.request.user
        schema.save()

        instances = formset.save(commit=False)
        for instance in instances:
            instance.schema = schema
            instance.save()

        for instance in formset.deleted_objects:
            instance.delete()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class SchemaListView(ListView, FilterQuerySetMixin):
    model = Schema
    paginate_by = 10



@method_decorator(login_required, name='dispatch')
class SchemaCreateView(SchemaColumnsMixin, CreateView):
    model = Schema
    form_class = SchemaForm

    def get_success_url(self):
        return reverse('schema-list')


@method_decorator(login_required, name='dispatch')
class SchemaDeleteView(DeleteView, FilterQuerySetMixin):
    model = Schema

    def get_success_url(self):
        return reverse('schema-list')


@method_decorator(login_required, name='dispatch')
class SchemaUpdateView(SchemaColumnsMixin, UpdateView, FilterQuerySetMixin):
    model = Schema
    form_class = SchemaForm

    def get_success_url(self):
        return reverse('schema-list')


@method_decorator(login_required, name='dispatch')
class DataSetListView(ListView, FormMixin, ProcessFormView):
    model = DataSet
    form_class = DataSetForm
    paginate_by = 10

    def get_queryset(self):
        return super().get_queryset().filter(schema=self.kwargs.get('pk'))

    def get_context_data(self, *, object_list=None, **kwargs):
        return {
            **super().get_context_data(object_list=object_list, **kwargs),
            'generator_form': self.get_form()
        }

    def form_valid(self, form):
        if form.is_valid():
            dataset = form.save(commit=False)
            dataset.schema = Schema.objects.get(pk=self.kwargs.get('pk'))
            dataset.status = Status.PROCESSING
            dataset.save()
            create_dataset.delay(dataset.id)
        return super().form_valid(form)

    def get_success_url(self):
        return self.request.path
