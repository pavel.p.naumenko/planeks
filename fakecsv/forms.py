from django import forms
from fakecsv.models import Schema, SchemaColumn, DataSet
from fakecsv.fields import MetaField


class SchemaForm(forms.ModelForm):
    class Meta:
        model = Schema
        fields = ('name', 'column_separator', 'string_character')


class SchemaColumnForm(forms.ModelForm):
    meta = MetaField(label='', required=False)

    class Meta:
        model = SchemaColumn
        fields = ('name', 'type', 'meta', 'order')


class DataSetForm(forms.ModelForm):
    class Meta:
        model = DataSet
        fields = ('rows',)
