from django.forms import MultiValueField, IntegerField
from fakecsv.widgets import MetaWidget


class MetaField(MultiValueField):
    widget = MetaWidget()

    def __init__(self, require_all_fields=False, **kwargs):
        fields = (
            IntegerField(label='From', show_hidden_initial=True, required=False),
            IntegerField(label='To', show_hidden_initial=True, required=False),
            IntegerField(label='# of Sentences', show_hidden_initial=True, required=False)
        )
        super().__init__(fields, require_all_fields=require_all_fields, **kwargs)

    def compress(self, data_list):

        from_val, to_val, sentences_val = data_list or [None, None, None]
        meta = {}
        if from_val in data_list:
            meta['from'] = from_val
        if to_val:
            meta['to'] = to_val
        if sentences_val:
            meta['sentences'] = sentences_val
        return meta
