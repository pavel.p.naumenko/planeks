from django.forms.widgets import MultiWidget, NumberInput
from django.utils.safestring import mark_safe


class MetaNumberInput(NumberInput):
    template_name = 'widgets/metanumberinput.html'


class MetaWidget(MultiWidget):

    def __init__(self, attrs=None, **kwargs):
        super(MetaWidget, self).__init__(widgets={
            'from': MetaNumberInput(attrs={'placeholder': 'From', 'label': 'From', 'col_class': 'from'}),
            'to': MetaNumberInput(attrs={'placeholder': 'To', 'label': 'To', 'col_class': 'to'}),
            'sentences': MetaNumberInput(
                attrs={'placeholder': '# of Sentences', 'label': '# of Sentences', 'col_class': 'sentences'}),
        }, **kwargs)

    def decompress(self, value):
        if isinstance(value, dict):
            return [value.get('from'), value.get('to'), value.get('length')]
        return [None, None, None]

    def _render(self, template_name, context, renderer=None):
        return mark_safe('<div class="row">' + super()._render(template_name, context, renderer) + '</div>')
