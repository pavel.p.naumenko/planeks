import random
from faker import Faker
from django.test import TestCase
from django.contrib.auth import get_user_model
from fakecsv.models import Schema, SchemaColumn, DataSet, Type
from fakecsv.services import DataFaker, FakeText, FakeInteger

User = get_user_model()

faker = Faker()


def user_factory():
    return User.objects.create(
        username=faker.user_name(),
        first_name=faker.first_name(),
        last_name=faker.last_name()
    )


def schema_factory(column_types: list):
    schema = Schema.objects.create(
        user=user_factory(),
        name=' '.join(faker.words(2))
    )
    for column_type in column_types:
        idx = column_types.index(column_type)
        schema.columns.create(
            name=str(column_type),
            type=column_type,
            order=idx
        )
    return schema


class ServiceTests(TestCase):

    def setUp(self):
        self.schema = schema_factory([
            Type.TEXT,
            Type.JOB,
            Type.ADDRESS,
            Type.FULL_NAME,
            Type.INTEGER,
            Type.EMAIL,
            Type.COMPANY_NAME,
            Type.DOMAIN_NAME,
        ])
        self.data_faker = DataFaker(self.schema)

    def test_data_faker_header_not_empty(self):
        headers = self.data_faker.get_headers()
        self.assertEqual(type(headers), list)
        self.assertEqual(len(headers), self.schema.columns.count())
        for header in headers:
            self.assertIsNotNone(header)
            self.assertNotEquals(header, '')

    def test_rows_count(self):
        rows_count = random.randint(0, 500)
        rows = self.data_faker.get_rows(rows_count)
        self.assertEqual(len(list(rows)), rows_count)

    def test_columns_count(self):
        row, = self.data_faker.get_rows(1)
        self.assertEqual(len(row), self.schema.columns.count())

    def test_fake_integer(self):
        meta = {'from': 100, 'to': 102}
        int_faker = FakeInteger(meta)
        val = int_faker.get_value()
        self.assertGreaterEqual(val, meta.get('from'))
        self.assertLessEqual(val, meta.get('to'))
