import time
from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class ColumnSeparator(models.TextChoices):
    COMMA = ','
    SEMICOLON = ';'
    TAB = '\t'
    SPACE = ' '


class EscapeChar(models.TextChoices):
    NONE = ''
    QUOTE = '\''
    DOUBLE_QUOTE = '"'


class Type(models.TextChoices):
    FULL_NAME = 'full_name'
    JOB = 'job'
    EMAIL = 'email'
    DOMAIN_NAME = 'domain_name'
    PHONE_NUMBER = 'phone_num'
    COMPANY_NAME = 'company_name'
    TEXT = 'text'
    INTEGER = 'integer'
    ADDRESS = 'address'
    DATE = 'date'


class Status(models.TextChoices):
    INITIAL = 'initial'
    PROCESSING = 'processing'
    READY = 'ready'


def directory_path(instance, filename):
    return f'dataset/{instance.schema.id}/{int(time.time())}_{filename}'


class Schema(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    column_separator = models.CharField(max_length=1,
                                        choices=ColumnSeparator.choices,
                                        default=ColumnSeparator.COMMA)
    string_character = models.CharField(max_length=1,
                                        blank=True,
                                        choices=EscapeChar.choices,
                                        default=EscapeChar.NONE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']


class SchemaColumn(models.Model):
    schema = models.ForeignKey(Schema, on_delete=models.CASCADE, related_name='columns')
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=12,
                            choices=Type.choices)
    meta = models.JSONField(null=True)
    order = models.IntegerField()

    class Meta:
        indexes = [
            models.Index(fields=['order'])
        ]


class DataSet(models.Model):
    schema = models.ForeignKey(Schema, on_delete=models.CASCADE, related_name='data_sets')
    file = models.FileField(max_length=256, upload_to=directory_path, null=True)
    status = models.CharField(max_length=12, choices=Status.choices)
    rows = models.IntegerField(default=100)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        indexes = [
            models.Index(fields=['id'])
        ]
