import io
import csv
from celery import shared_task
from django.core.files.base import ContentFile
from fakecsv.models import DataSet, Status, EscapeChar
from fakecsv.services import DataFaker


@shared_task
def create_dataset(dataset_id):
    dataset = DataSet.objects.get(pk=dataset_id)
    schema = dataset.schema
    faker = DataFaker(schema)
    csv_buffer = io.StringIO()
    params = {
        'delimiter': schema.column_separator
    }
    if schema.string_character != EscapeChar.NONE:
        params['quotechar'] = str(schema.string_character)
    else:
        params['quoting'] = csv.QUOTE_NONE
        params['escapechar'] = '\\'
    writer = csv.writer(csv_buffer, dialect='excel', **params)
    writer.writerows([faker.get_headers()])
    writer.writerows(list(faker.get_rows(dataset.rows)))
    file = ContentFile(csv_buffer.getvalue().encode('utf-8'))
    dataset.file.save('dataset.csv', file)
    dataset.status = Status.READY
    dataset.save()
