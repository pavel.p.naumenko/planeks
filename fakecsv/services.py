import random
from faker import Faker
from fakecsv.models import Type
from typing import Generator, Callable

fake = Faker()


class FakeValue:
    method: Callable
    meta: dict = {}

    def __init__(self, meta):
        self.meta = meta or {}

    def get_value(self):
        return self.method()


class FakeName(FakeValue):
    method = fake.name


class FakeAddress(FakeValue):
    method = fake.address


class FakeEmail(FakeValue):
    method = fake.email


class FakeDomainName(FakeValue):
    method = fake.domain_name


class FakeDate(FakeValue):
    method = fake.domain_name


class FakeInteger(FakeValue):

    def get_value(self):
        return random.randint(self.meta.get('from', 0), self.meta.get('to', 9999))


class FakeCompany(FakeValue):
    method = fake.company


class FakeJob(FakeValue):
    method = fake.job


class FakePhone(FakeValue):
    method = fake.phone_number


class FakeText(FakeValue):
    def get_value(self):
        return ''.join([fake.sentence() for _ in range(self.meta.get('sentences', 1))])


fakers = {
    (Type.INTEGER): FakeInteger,
    (Type.ADDRESS): FakeAddress,
    (Type.DATE): FakeDate,
    (Type.DOMAIN_NAME): FakeDomainName,
    (Type.EMAIL): FakeEmail,
    (Type.FULL_NAME): FakeName,
    (Type.COMPANY_NAME): FakeCompany,
    (Type.JOB): FakeJob,
    (Type.PHONE_NUMBER): FakePhone,
    (Type.TEXT): FakeText,
}


class DataFakerException(Exception):
    pass


class DataFaker:
    schema = None

    def __init__(self, schema):
        self.schema = schema

    def get_headers(self) -> list[str]:
        return [row.name for row in self.schema.columns.all()]

    @staticmethod
    def get_faker(faker_type: Type):
        if faker_type not in fakers:
            raise DataFakerException(f'Faker isn\'t declared for {faker_type}')
        return fakers[faker_type]

    def get_fakers(self) -> list[FakeValue]:
        fakers_list = []
        for column in self.schema.columns.all():
            fake_value_vlass = self.get_faker(column.type)
            fakers_list.append(fake_value_vlass(column.meta))
        return fakers_list

    def get_rows(self, rows) -> Generator:
        fakers_list = self.get_fakers()
        for _ in range(0, rows):
            yield [faker.get_value() for faker in fakers_list]
