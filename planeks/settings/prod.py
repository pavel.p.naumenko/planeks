# pylint: disable-msg=w0614
# flake8: noqa
from planeks.settings.base import *

DEBUG = False

ALLOWED_HOSTS = ['cryptic-bastion-07859.herokuapp.com']

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
