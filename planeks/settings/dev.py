# pylint: disable-msg=w0614
# flake8: noqa
from planeks.settings.base import *

ALLOWED_HOSTS = ['localhost']

CELERY_TASK_ALWAYS_EAGER = True
CELERY_TASK_EAGER_PROPAGATES = True
