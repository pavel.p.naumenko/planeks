"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView
from fakecsv.views import SchemaListView, SchemaCreateView, SchemaUpdateView, SchemaDeleteView, DataSetListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('', SchemaListView.as_view(), name='schema-list'),
    path('schema/', SchemaCreateView.as_view(), name='schema'),
    path('schema/<int:pk>/', SchemaUpdateView.as_view(), name='schema-detail'),
    path('schema/<int:pk>/delete/', SchemaDeleteView.as_view(), name='schema-delete'),
    path('schema/<int:pk>/dataset/', DataSetListView.as_view(), name='dataset-list'),
    *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
]
