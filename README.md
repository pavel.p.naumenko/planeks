# Local Development

Use Docker Compose for local development. Just try, if you don't know about it, you'll like it ;)

## Makefile

If you are working *nix systems or MacOS, you can use `make` commands, they make your life a little bit easier.
Examples: 

 - `make run`
 - `make makemigrations`

Look int `./Makefile` for more examples

# Heroku

`heroku ps:scale web=1`

`heroku open`
